<?php
header('Access-Control-Allow-Origin:*');
header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
header("Access-Control-Allow-Headers: content-type,authorization");
header("Access-Control-Allow-Credentials: true");

if(!empty($_POST['api'])){
	$json['code'] = 200;
	$json['msg'] = '请求成功';
}else{
	$json['code'] = 1000;
	$json['msg'] = '请求失败';
}

echo json_encode($json);


